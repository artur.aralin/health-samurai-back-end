(ns health-samurai-back-end.components.patient.test
  (:require
   [clojure.test :refer [deftest testing is]]
   [health-samurai-back-end.components.patient.controller :as ctrl]))

(deftest patient-controller
  (testing "get-patients"
    (is (let [res (ctrl/get-patient {:params {:limit 10
                                              :offset 0}})
              body (:body res)
              status (:status body)]
          (= :ok status)))

    (is (let [res (ctrl/get-patient {:params {:limit "dsa"
                                              :offset 0}})
              body (:body res)
              status (:status body)]
          (= :bad-request status)))))
