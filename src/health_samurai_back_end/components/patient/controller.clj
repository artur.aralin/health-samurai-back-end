(ns health-samurai-back-end.components.patient.controller
  (:require
   [ring.util.response :refer [response]]
   [tools.routing :refer [with-validation]]
   [health-samurai-back-end.components.patient.schemas :as schema]
   [db.tools :refer [nil-to-null-keyword parse-date-in]]
   [health-samurai-back-end.components.patient.model :as PatientModel]))

(def get-patient
  (with-validation
    schema/get-patients
    (fn [req]
      (let [params (reduce
                    (fn [x k] (update-in x [k] #(Integer. %)))
                    (get-in req [:params])
                    [:limit :offset])]
        (response
         {:status :ok
          :total (PatientModel/count-patients!)
          :response (PatientModel/get-all! params)})))))

(def create-patient
  (with-validation
    schema/create-patient
    (fn [req]
      (response
       {:status :ok
        :response (->> (get-in req [:body :patients])
                       (map #(PatientModel/PatientRecord
                              (merge {:address ""} %)))
                       PatientModel/create-patients!)}))))

(def update-patient
  (with-validation
    schema/update-patient
    (fn [req]
      (doall
       (->> (get-in req [:body :update_patients])
            (map #(-> % nil-to-null-keyword (parse-date-in [:birthday])))
;; here must be a tansaction
            (map PatientModel/update-patient!)))
      (response {:status :ok}))))

(def delete-patient
  (with-validation
    schema/delete-patients
    (fn [req]
      (doall
       (PatientModel/delete-patients-by-id!
        (->> (get-in req [:body :ids])
             (map #(Integer. %)))))
      (response {:status :ok}))))
