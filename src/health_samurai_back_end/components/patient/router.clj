(ns health-samurai-back-end.components.patient.router
  (:require
   [compojure.core :refer [routes GET POST PUT DELETE]]
   [health-samurai-back-end.components.patient.controller :as ctrl]))

(def router
  (routes
   (GET "/patients" req (ctrl/get-patient req))
   (POST "/patients" req (ctrl/create-patient req))
   (PUT "/patients" req (ctrl/update-patient req))
   (DELETE "/patients" req (ctrl/delete-patient req))))
